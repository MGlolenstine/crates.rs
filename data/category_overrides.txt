abortable_parser: parsing
ace: command-line-interface, parser-implementations
ackorelic: development-tools::debugging, network-programming
acm: science::math
actions-core: web-programming
advancedresearch-higher_order_core: rust-patterns
aerosol: rust-patterns
afterburn: web-programming
afterparty-ng: web-programming::http-server
ag: command-line-utilities
age-core: cryptography
ai_kit: science::machine-learning
alass-core: algorithms
alert-after: gui, command-line-utilities
alfred: development-tools, os::macos-apis
algorust: algorithms
allegro_primitives-sys: game-engines
allegro_util: game-engines
almost: rust-patterns, no-std
alog: parser-implementations, web-programming, command-line-utilities
alphaid: encoding
alphavantage: web-programming
amath: parser-implementations
anes: parser-implementations, command-line-interface
anitomy: parser-implementations
anybar_rs: os::macos-apis
aocl: command-line-utilities
approvals: development-tools::testing
apt-cache: os::unix-apis
aquaenv: os::macos-apis, command-line-utilities, gui
arcon: science
argh: command-line-interface
arthas: database-implementations
ascon: cryptography
asn1rs: parser-implementations, encoding
ass-rs: web-programming
ast_node: parser-implementations
async-codec: asynchronous
atomicring: concurrency, data-structures
atomsh: command-line-utilities
augr-core: command-line-utilities
azul-simplecss: parser-implementations, web-programming
background-jobs-core: asynchronous
background-jobs-server: asynchronous
balena-temen: parser-implementations
bao: algorithms
barfly: gui
baseperm: algorithms
bb8: asynchronous
bcc-sys: development-tools
bcc: development-tools
bcfs: cryptography::cryptocurrencies, wasm
beacon: cryptography::cryptocurrencies
begonia: command-line-interface
bfc: emulators
bigbro: os, filesystem
billecta: web-programming
binaryen-sys: wasm
binny: parser-implementations, multimedia
binwrite: encoding
bio-types: science, rust-patterns
bio: science, algorithms, data-structures
bitcoin_hashes: cryptography::cryptocurrencies
bitcoincore-rpc: cryptography::cryptocurrencies
bitvector: data-structures
blip_buf: multimedia::audio
blockchain-network-simple: cryptography::cryptocurrencies
bloke: game-engines
bloomfilter: data-structures
bnf: parsers
boxcars: parser-implementations
br4infuck: emulators
brain-brainfuck: emulators, command-line-utilities
brain-rs: emulators
brain: emulators
brainpreter: emulators
bresenham: rendering::engine, algorithms
bright: command-line-utilities
bs58: encoding
bspline: rendering
btc_spv_bin:cryptography::cryptocurrencies
buffered-reader: rust-patterns
buffett-core: web-programming
c2rust-ast-exporter: value-formatting, encoding
cage: development-tools, command-line-utilities
calx: os::macos-apis, multimedia::audio
cansi: parser-implementations
capstone-sys: development-tools::debugging
capstone3: development-tools::debugging
captcha: web-programming
carapax  : web-programming
cargo-xbuild: development-tools::build-tools, development-tools::cargo-plugins
casperlabs-contract-ffi: cryptography::cryptocurrencies
casperlabs-contract: cryptography::cryptocurrencies
casperlabs-engine-grpc-server: cryptography::cryptocurrencies
casperlabs-engine-test-support: cryptography::cryptocurrencies
casperlabs-types: cryptography::cryptocurrencies
cb2: command-line-utilities
celery-rs-core: asynchronous
celery: asynchronous
cgl: rendering::graphics-api, os::macos-apis
ch8asm: emulators, development-tools
chariot_slp: games, parser-implementations
checkm: science, parser-implementations
chillup: web-programming
chrobry-core: template-engines
chromatica: multimedia::images
cht: concurrency, data-structures
cita_trie: data-structures
clargs: command-line-interface
classreader: parser-implementations, development-tools
click: development-tools
clocksource: date-and-time, os
coap: network-programming
codec-impl: cryptography::cryptocurrencies
codenano: science
codespan-lsp: encoding
collider: science::math, game-engines
collisions: algorithms
colloquy: command-line-interface
colorsys: multimedia::images
commoncrypto-sys: os::macos-apis
commoncrypto: os::macos-apis, cryptography
concurrent-hashmap: concurrency, data-structures
conllx: parser-implementations
contralog: development-tools::debugging
conv: rust-patterns
core-foundation-sys: os::macos-apis
core-graphics: os::macos-apis, rendering::graphics-api
core-nightly: development-tools
core-text: os::macos-apis
core-video-sys: os::macos-apis
coreaudio-sys: os::macos-apis, multimedia::audio
coremidi: os::macos-apis, multimedia::audio
cov: parser-implementations, development-tools::debugging
coverm: science
cox: web-programming
coz: development-tools::profiling
cqc: simulation
cqrs-core: asynchronous
crate-race: development-tools::profiling
crates-index-diff: development-tools
crates_io_api: web-programming
cron-parser: parser-implementations
crossterm_style: command-line-interface
crossterm_utils : command-line-interface
crunchy: development-tools
cryptoballot: cryptography, authentication, gui
cryptonote-wallet: cryptography::cryptocurrencies
csound: multimedia::audio
css-color: parser-implementations, web-programming
cssparser: parser-implementations, web-programming
cuckoocache: concurrency, caching, data-structures
cute-dnd-dice: game-engines
cvssrust: parser-implementations
cxmr-util-servers : web-programming::http-server
darling_core: development-tools::procedural-macro-helpers
ddh: filesystem, command-line-utilities
deadpool-redis : databases
debug_finder: parser-implementations, command-line-utilities
deckofcards: game-engines
default-env: rust-patterns
delegatemethod: rust-patterns
deltae: multimedia
deltamap: command-line-utilities, science
deno: web-programming, development-tools
deno_cli_snapshots: command-line-utilities, development-tools
deno_core: web-programming, development-tools
deno_lib_bindings: development-tools::ffi
deno_typescript: web-programming, development-tools
deoxy-core: data-structures
deribit: web-programming
derp: parser-implementations
derpy: development-tools
dfw: development-tools, network-programming
dia-args: command-line-interface
diceware-rs: command-line-utilities, authentication
diceware: command-line-utilities, authentication
dicom-core: multimedia::images
diesel: databases
differential-geometry: science::math
direction: rust-patterns
ditto: concurrency, data-structures
dmg: os::macos-apis, filesystem
dockerfile-parser: parser-implementations
docstrings: parser-implementations
docx: parser-implementations, text-processing
doryen-fov: game-engines
doublepivot-quicksort: algorithms
doubter: development-tools::testing
dull: command-line-utilities, command-line-interface
dungen: game-engines
duniter-keys: cryptography::cryptocurrencies
duniter-wotb: cryptography::cryptocurrencies
durs-network-documents: cryptography::cryptocurrencies
durs-wot: cryptography::cryptocurrencies
dwarf-term: command-line-interface, game-engines
dwm-status: os, gui
dynamic: rust-patterns
dynamic_reload: os, development-tools
dynparser: parsing
easy-plugin: development-tools
easy_adds: rust-patterns
easyunits: rust-patterns
eclectica: development-tools
eetf: encoding
egaku2d_core: rendering::graphics-api, game-engines
elefren: web-programming
emerald-vault-core: cryptography::cryptocurrencies
emlop: parser-implementations
emmett: parser-implementations
endian-hasher: algorithms
endicon: algorithms
eng-pwasm-abi: cryptography::cryptocurrencies, wasm
eng-wasm: cryptography::cryptocurrencies, wasm
entity_store_code_gen: game-engines
entity_store_helper: game-engines
env-io: rust-patterns
env_proc: config
env_proc: rust-patterns, config
envoption: config
eosio: cryptography::cryptocurrencies
eosio_macros_internal: cryptography::cryptocurrencies
eosio_numstr: cryptography::cryptocurrencies
eraserhead: rust-patterns
esplugin: parser-implementations
etclient-core: cryptography::cryptocurrencies
euclid: science::math
evcxr_repl: rust-patterns, command-line-utilities
evolution_rs: science::machine-learning
ewasm_api: wasm
exonum-cli: cryptography::cryptocurrencies
exonum-configuration: cryptography::cryptocurrencies
exonum-cryptocurrency-advanced: cryptography::cryptocurrencies
exonum-testkit: cryptography::cryptocurrencies
exonum-time: cryptography::cryptocurrencies
exonum: cryptography::cryptocurrencies
failure: rust-patterns
fakesite: web-programming
falcon-mos6502: emulators
falcon-raptor: development-tools
fast-floats: rust-patterns
fast-socks5: network-programming, web-programming::http-client, web-programming::http-server
fast_uaparser: parser-implementations
fastcgi-client: asynchronous, web-programming::http-client
fastdl: caching
favy: multimedia::images
fddf: command-line-utilities, filesystem
fdfast: filesystem
fec: encoding
fedora-prime: os::unix-apis, command-line-utilities
ff: algorithms
filetime: filesystem
fin_data: value-formatting
finality-grandpa:cryptography::cryptocurrencies
finchers-core: web-programming::http-server
find-files: filesystem
findomain: network-programming
findomain: networking
finite: science::math
finn-assembler: development-tools
fitrs: parser-implementations
fixed_width: parser-implementations
fixedstep: game-engines
fixedvec: no-std, data-structures
flats: data-structures
flightmath: command-line-utilities, science
float-ord: rust-patterns
fluminurs: development-tools, web-programming
fluxer: command-line-utilities, multimedia::images
fma: rust-patterns, hardware-support
follow: command-line-utilities
ForceFeedback-sys: os::macos-apis,hardware-support
foreignc_derive: development-tools::procedural-macro-helpers
forever: rust-patterns
forismatic: web-programming
fp-core: development-tools
fractal-matrix-api: network-programming
freeimage: multimedia::images, os::unix-apis
frotate: command-line-utilities, os
frugalos_core: os
fse: algorithms
fsevent-sys: os::macos-apis
fsevent: os::macos-apis
ftoa: value-formatting
full_moon: parser-implementations, development-tools
futures-core: asynchronous
futures-executor: asynchronous
futures-task: asynchronous
g13-rs: hardware-support
ga-v4-flattener: web-programming
ga: science::machine-learning, algorithms
galah: science
gallop: parsing
game-loop: game-engines
gary-core: development-tools
gaviota-sys: game-engines
gcnctrlusb: hardware-support, emulators
gdnative-video: game-engines, multimedia::video
gdnative_bindings_generator: game-engines
geckodriver: development-tools::testing
generic-core: encoding
genp: authentication
genpass: authentication, command-line-utilities
geoplaces: science
geoplaces: science, web-programming
geos: science::math
getch: os
giphy: web-programming
girage: cryptography, gui
github-rs: web-programming
gitlab-ci-validate : development-tools
global_counter: concurrency, development-tools::debugging
glsl: rendering, parser-implementations
glutin_cocoa: os::macos-apis
glutin_core_foundation: rendering::graphics-api
glx: rendering::graphics-api, os::unix-apis
google_geocoding: science, web-programming
gosh-core: science
graftpress_core: web-programming
grammarly: web-programming
graphcore: data-structures
graviton_core: development-tools
grin-miner: cryptography::cryptocurrencies
grin: cryptography::cryptocurrencies
grin_api: cryptography::cryptocurrencies
grin_core: cryptography::cryptocurrencies
grin_keychain: cryptography::cryptocurrencies
grin_store: cryptography::cryptocurrencies
grin_wallet_impls: cryptography::cryptocurrencies
gtfs-structures: parser-implementations
gtrans: command-line-utilities, internationalization
gwasm-dispatcher: wasm
hash-db: rust-patterns, databases, no-std
hash256-std-hasher: rust-patterns, no-std
hashmap_core: data-structures
hdf5: data-structures
hdlc: network-programming
hdwallet-bitcoin: cryptography::cryptocurrencies
heliometer: emulators
hexe_core: game-engines
holochain:cryptography::cryptocurrencies
holochain_common:cryptography::cryptocurrencies
holochain_conductor_lib:cryptography::cryptocurrencies
holochain_conductor_lib_api:cryptography::cryptocurrencies
holochain_core:cryptography::cryptocurrencies
holochain_dpki:cryptography::cryptocurrencies
holochain_json_derive:cryptography::cryptocurrencies
holochain_locksmith:cryptography::cryptocurrencies
holochain_logging:cryptography::cryptocurrencies
holochain_metrics:cryptography::cryptocurrencies
holochain_persistence_api:cryptography::cryptocurrencies
httpdate: date-and-time, parser-implementations
httpserv: development-tools, web-programming::http-server
hubcaps: web-programming
humanize-rs: parser-implementations, value-formatting
hybrid-clocks: algorithms
hyper-router: web-programming::http-server
i3_ipc: network-programming
i3ipc: hardware-support
iban_validate: parser-implementations
ical: parser-implementations
igdb-rs: games
ihex: embedded, development-tools
ilibipt: hardware-support
image-capture-core: os::macos-apis
image-interlacer: multimedia::images, command-line-utilities
imdb-index: web-programming
imdb: web-programming
impl-codec: cryptography::cryptocurrencies
indradb-lib: database-implementations
indradb: database-implementations
indy-sys:cryptography::cryptocurrencies
infer: filesystem
inheritance: rust-patterns
iostream: data-structures
iota-constants: cryptography::cryptocurrencies
iota-conversion: cryptography::cryptocurrencies
iota-crypto: cryptography::cryptocurrencies
iota-lib-rs-preview: cryptography::cryptocurrencies
iota-model: cryptography::cryptocurrencies
iota-pow: cryptography::cryptocurrencies
iota-spammer: cryptography::cryptocurrencies
iota-utils: cryptography::cryptocurrencies
ipc-orchestrator: command-line-utilities, asynchronous
ipc-rs: asynchronous, os
ipc: asynchronous
ipp-headers-sys: hardware-support
ipp-sys: hardware-support, development-tools::profiling
ipwhois: databases, network-programming
is-macro: development-tools::procedural-macro-helpers
isbfc: emulators
iso639-1: internationalization
iso639_2: internationalization
iso8601-duration: date-and-time, parser-implementations
iso_currency: internationalization
jeepers: science::machine-learning
jokeyrhyme-dotfiles: development-tools
json-tools: parser-implementations
json2cbor: encoding, command-line-utilities
jsonapi: web-programming
jsonpath_lib: parser-implementations
jsonrpc-client-core: parser-implementations
jsonrpc-codec: web-programming
jsonrpc-core-client: web-programming
jsonrpc-core: web-programming
jsonrpc-parse: parser-implementations, web-programming
jsonrpc-v2: web-programming::http-server
jsonsv: encoding
jubjub: algorithms
just: development-tools, command-line-utilities
k2hash-sys: databases
k8-client: rust-patterns
k8s-openapi-codegen-common: rust-patterns
k8s-openapi: development-tools
kekbit-codecs: encoding
ketos: development-tools
keychain-services: os::macos-apis, authentication
kf-protocol-transport: asynchronous, network-programming
kf-protocol: asynchronous, network-programming
kf-socket: network-programming
kickstart : development-tools, command-line-utilities
kodict: internationalization
kraken: authentication
krates: visualization
krust_core: game-engines
ktmpl: configuration
kube: development-tools
kurbo: science::math, rendering::graphics-api
kx: development-tools
lapp: parser-implementations
lava_torrent: parser-implementations
lcov: parser-implementations
lde: parser-implementations, hardware-support
leafrender: gui, rendering::graphics-api
ledger-parser: parser-implementations
letter-avatar: multimedia::images
lib_sens: game-engines
libass: web-programming
libblas-sys: science::math
libdoh: network-programming, asynchronous
libinput-sys: os
liblapack-sys: science::math
libloadorder-ffi: game-engines
libnghttp2-sys: web-programming::http-server
librdkafka-sys: asynchronous
librespot-core: multimedia::audio
librsyncr: network-programming
libsbc-sys: hardware-support
libtimew: parser-implementations
ligen-core: development-tools
lightning: cryptography::cryptocurrencies
lightspeed_core: web-programming
line: text-editors
line_drawing: game-engines, algorithms
link-ippcore : hardware-support
link-ippi: hardware-support
llvm-passgen: development-tools
loc: text-processing
loc: text-processing, command-line-utilities
locationsharing: science, web-programming
log_domain: science::math
log_kv: database-implementations
loggify: development-tools::debugging
loop-rs: command-line-utilities
lsp-codec: asynchronous
lttb: algorithms
lualexer: parser-implementations
lumberjack-utils: data-structures, command-line-utilities
lumberjack: data-structures
lux: game-engines
lv2-core: multimedia::audio
lv2-units: multimedia::audio, rust-patterns
lv2-urid-derive: development-tools::procedural-macro-helpers
lv2: multimedia::audio
lv2_raw: multimedia::audio
lv2rs-core: multimedia::audio
lv2rs: multimedia::audio
lw-webdriver: development-tools::testing
lyon_core: multimedia::images, rendering::graphics-api
mac: rust-patterns
macchanger: os, network-programming
mach: os::macos-apis
magenta-sys: os
magenta: os
magic-wormhole: command-line-utilities, network-programming
makeflow: development-tools
makepad-widget: web-programming
makepad: text-editors
mamba: parser-implementations
marge : development-tools
marionette: development-tools::testing
markdust: text-processing
matrix_bot_api: network-programming
mdtoc: text-processing
mediawiki: web-programming
memorydb: database-implementations
mermer-rs: text-processing, science
meson: development-tools::build-tools
metac: parser-implementations, development-tools
metar: parser-implementations
mindjuice: emulators
mine_sweeper: games
minidump-processor:development-tools::debugging
minimad: text-processing, parser-implementations
miniquad: gui
mlc: command-line-utilities, text-processing
mnemonic: command-line-utilities, authentication
mobi: parser-implementations
mockery: development-tools::testing
modbot: web-programming
modbus-core: network-programming
moltenvk_deps: os::macos-apis, rendering::graphics-api
monger: databases
moonlander-gp: science::machine-learning
mothra: command-line-utilities
mp3-duration-sum: command-line-utilities, multimedia::audio
mqtt-codec: network-programming
mrt-rs: parser-implementations, network-programming
msql-srv: database
multiboot2: os
mxruntime-sys: os
mxruntime: os
mycelium_core: database-implementations
myelin-geometry: science::math
nan-preserving-float: rust-patterns
nat_traversal: network-programming
naturalize: value-formatting
nazar: science, web-programming
nd: web-programming, command-line-utilities
nd_lib: web-programming
ndless-static-vars: rust-patterns
near-bindgen-core :cryptography::cryptocurrencies
near-bindgen-core: cryptography::cryptocurrencies
near-bindgen-macros :cryptography::cryptocurrencies
near-bindgen-promise: cryptography::cryptocurrencies
near-bindgen: cryptography::cryptocurrencies
near-bindgen:cryptography::cryptocurrencies
near-rpc-error-macro:cryptography::cryptocurrencies
near-runtime-fees: cryptography::cryptocurrencies
near-runtime-fees:cryptography::cryptocurrencies
near-sdk : cryptography::cryptocurrencies
near-sdk-core : cryptography::cryptocurrencies
near-sdk-macros : cryptography::cryptocurrencies
near-sdk: cryptography::cryptocurrencies
near-vm-errors: cryptography::cryptocurrencies
near-vm-errors:cryptography::cryptocurrencies
near-vm-logic: cryptography::cryptocurrencies
near-vm-runner-standalone:cryptography::cryptocurrencies
neli: network-programming, os::unix-apis
neocom: emulators
neon-sys: web-programming
neovim: text-editors
nes: rust-patterns
nessus: development-tools, network-programming
nestools: game-engines, emulators
netdevice: os, network-programming
netrc-rs: parser-implementations
new-ordered-float: rust-patterns
newrelic: development-tools
newrelic_plugin: web-programming
newt: command-line-interface
nite2-sys: science::machine-learning
nokiahealth: databases
nordselect: network-programming, command-line-utilities
notcurses: command-line-interface
notes: command-line-utilities
notice-core: asynchronous
notif: gui
nq: development-tools, command-line-utilities
ntriple: parser-implementations
num_sys: value-formatting
nuscenes-data: parser-implementations
nv-xml: parser-implementations
nvim-rs: text-editors
nvim_windows_remote: text-editors
nzsc2p: games
nzsc_core: game-engines
objc-encode: os::macos-apis, development-tools::ffi
olekit: parser-implementations
onig_sys: text-processing
ontio-bump-alloc: cryptography::cryptocurrencies
openhmd-rs-sys: multimedia, rendering::graphics-api
openldap: network-programming
openni2: science::machine-learning
openpol: games
orgize: parser-implementations
osmesa-sys: multimedia, os::unix-apis
osmgpsmap: science, gui
ostrich-core: asynchronous
overdose: concurrency, data-structures
ovgu-canteen: parser-implementations
oxygengine-core: game-engines, wasm
packapp: os::macos-apis, command-line-utilities
pallet-contracts: cryptography::cryptocurrencies
papyrus: development-tools, command-line-utilities
parity-scale-codec-derive: cryptography::cryptocurrencies
parse-display-derive: rust-patterns, development-tools::procedural-macro-helpers
parse-display: rust-patterns
parse_cfg: parser-implementations, rust-patterns
parse_duration: parser-implementations
parse_float_radix: parser-implementations
passclip: command-line-utilities
pathfinder_geometry: science::math
paw-attributes: development-tools::procedural-macro-helpers
paw: parser-implementations
pcd-rs: parser-implementations
pdcurses-sys: command-line-interface
perkv: database-implementations
persawkv: database-implementations
peruse: parsing
pf: filesystem, network-programming
pickpocket: web-programming
pico-sys: web-programming
pinentry: command-line-interface, authentication
pkgman: os
plagiarismbasic_lib: text-processing
playlist-decoder: parser-implementations, multimedia::audio
playlist-duration: command-line-utilities, multimedia::audio
plugger-core: development-tools::ffi
pngcrush: compression
polygraph: database-implementations
pomodoro: command-line-utilities
portaudio: os, multimedia::audio
postscript: parser-implementations
powerline-rs : development-tools, command-line-utilities
ppg: command-line-utilities, authentication
pq-sys: database
preftool-env: config
pretty: text-processing, command-line-interface
proc-concat-bytes: data-structures, rust-patterns
prodash: asynchronous, visualization
project-cleanup: command-line-utilities, development-tools
prometheus-parser: parser-implementations
prometheus_wireguard_exporter: network-programming
psyche-core: science::machine-learning
pwasm-abi-derive: cryptography::cryptocurrencies
pwasm-abi: cryptography::cryptocurrencies
pwasm-ethereum: cryptography::cryptocurrencies
pwasm-libc: cryptography::cryptocurrencies
pwasm-std: cryptography::cryptocurrencies
pwasm-utils: cryptography::cryptocurrencies
qeda: command-line-utilities
qni-core-rs: web-programming::websocket
quaternion_averager: science::math
quicksort: algorithms
quoted_printable: parser-implementations
qupla: cryptography::cryptocurrencies
qwerty: command-line-utilities, authentication
rair: command-line-utilities, development-tools
randnum: command-line-utilities
randrust: web-programming::http-server
raptorq: network-programming
rascam: hardware-support, multimedia::video
ratio: science::math
rcore: development-tools
rdispatcher: concurrency
rdkafka: asynchronous, network-programming
reader_for_microxml: parser-implementations
readmouse: os::macos-apis
recloser: concurrency
recoreco: command-line-utilities, science::machine-learning
redis-cluster: databases
redisearch_api: databases
releasetag: development-tools::debugging
rendy-memory: memory-management
repo-backup : development-tools, command-line-utilities
repoutil: development-tools
reproto-backend-csharp: encoding, development-tools::ffi
reproto-backend-doc: encoding, development-tools::ffi
reproto-backend-go: encoding, development-tools::ffi
reproto-backend-java: encoding, development-tools::ffi
reproto-backend-js: encoding, development-tools::ffi
reproto-backend-json: encoding, development-tools::ffi
reproto-backend-python: encoding, development-tools::ffi
reproto-backend-reproto: encoding, development-tools::ffi
reproto-backend-rust: encoding, development-tools::ffi
reproto-backend-swift: encoding, development-tools::ffi
reproto-backend: encoding, network-programming
reproto-core: encoding, network-programming
reproto-env: encoding, network-programming
reproto-repository-http: encoding, network-programming
reproto-repository: encoding, network-programming
ress: parser-implementations
ressa: parser-implementations
result-inspect: rust-patterns
retrosheet: parser-implementations
rettle: databases
retworkx: data-structures
rfind_url: parser-implementations
rg3d-core: rendering::graphics-api, game-engines
rgraph: concurrency
rhq-core: development-tools
ritual_build: development-tools, command-line-utilities
ritual_common: development-tools::ffi
riven: games
rliron: game-engines
rmatrix: command-line-utilities
rnix: parser-implementations
roadmap: data-structures
roads-from-nd: multimedia::images
robinhood: web-programming
rocket-etag-if-none-match: web-programming::http-server
rojo: development-tools
romodoro: command-line-utilities
romy-core: game-engines
romy-wasmer: game-engines
romy: game-engines
rosalind: algorithms, science
rosy: development-tools::ffi
roulette-wheel: science::machine-learning, algorithms
round: rust-patterns
roux: web-programming
rpi-async: hardware-support, asynchronous
rpi-led-matrix: hardware-support
rpi-video-rs: hardware-support, multimedia::video
rpm-rs: os, parser-implementations
rqr: multimedia::images
rsdb: database-implementations
rtm: concurrency, hardware-support, no-std
rubrail: os::macos-apis
rumq-core: network-programming
rusoto_credential: authentication
rusp: network-programming
rust-easy-router: web-programming::http-server
rust_fixed_width: data-structures
rustasm6502: emulators
rustils: rust-patterns
rustler_sys: development-tools::ffi
rustop-rs: command-line-utilities, os::unix-apis
rustration: emulators
rustup-find: rust-patterns, command-line-utilities
rustwtxt: parser-implementations
rusty_sword_arena: game-engines
rustydav: web-programming
ruut: command-line-utilities, value-formatting
rxrust: asynchronous
salad: command-line-utilities, authentication
saml2aws-auto: authentication
sample_planning: science::machine-learning, algorithms
sass-sys: web-programming
scan4df: command-line-utilities, filesystem
scannit-core-ffi: development-tools::ffi
scannit-core-ffi: hardware-support
scannit-core: web-programming
scdlang: parser-implementations
scoped-env: config
scrapmetal: rust-patterns, algorithms
sdkms: hardware-support
seahorse: command-line-interface
sekai: science
semantic-rs: development-tools
semver-parser: parser-implementations
serenium: development-tools::testing
sfl_parser: parser-implementations
sgf-parser: parser-implementations
shellfn-core: command-line-interface
shippai:rust-patterns
shuteye: os, date-and-time
sic_core: multimedia::images
sigv4: command-line-utilities, web-programming
simple-ssl-acme-cloudflare: cryptography, command-line-utilities
simple_json: parser-implementations
single_source: text-processing, command-line-utilities
sip-codec: network-programming
sitemap: parser-implementations
slot: memory-management
smalltree: data-structures
smileypyramid: command-line-utilities
snoopy: parser-implementations, network-programming
solana-clap-utils:cryptography::cryptocurrencies
solana-client:cryptography::cryptocurrencies
solana-gossip:cryptography::cryptocurrencies
solana-keygen:cryptography::cryptocurrencies
solana-logger:cryptography::cryptocurrencies
solana-vote-program: cryptography::cryptocurrencies
solid-core: cryptography::cryptocurrencies
solid: cryptography::cryptocurrencies
solsa: cryptography::cryptocurrencies
sorted-json: encoding
sp-std: cryptography::cryptocurrencies
spade: data-structures
specs_sprite: game-engines
speculo: development-tools
spinning_top: concurrency
splinter: cryptography::cryptocurrencies
sqlparser: parser-implementations
ssb_parser: parser-implementations
sse-codec: web-programming
stamm: science::machine-learning
star-history: command-line-utilities
statsd-parser: parser-implementations
stemjail: os::unix-apis
stringlit: rust-patterns
sulfur: development-tools::testing
suspicious-pods: development-tools
susy-codec-derive:cryptography::cryptocurrencies
sv-parser: parser-implementations
svd-parser: parser-implementations
svgparser: parser-implementations, rendering::data-formats
sw-composite: rendering
swc_atoms: parser-implementations
swc_common: parser-implementations
swc_ecma_ast: parser-implementations
swc_ecma_parser: parser-implementations
swc_ecma_parser_macros: parser-implementations
swears: command-line-interface
symbolic-common: development-tools::debugging
symbolic-debuginfo: development-tools::debugging
symbolic-minidump: development-tools::debugging
symbolic: development-tools::debugging
syncat-stylesheet: parser-implementations
sys-info: os, filesystem
sys_util: os
table-extract: parser-implementations
tagref: development-tools
taildir: filesystem
tarkov: games
tcalc: command-line-utilities
tdo-core:command-line-interface
tealdeer: command-line-utilities
telegram-bot : web-programming
telegram-bot-async  : web-programming
telegram-bot-fork : web-programming
telegram-client: web-programming
telegram-typings  : web-programming
telegrambot  : web-programming
telnetify: network-programming
teloxide: web-programming
tempus_fugit: development-tools::profiling
termcodes: command-line-interface, no-std
text_io: text-processing
tgaimage: multimedia::images
thingvellir: concurrency
thrift_codec: encoding, web-programming
time-parse: date-and-time, parser-implementations
timeago: date-and-time
tls_read_hancock_bin: parser-implementations, science
tmux-thumbs: text-editors
toks: parser-implementations, web-programming
trackable: development-tools::debugging
traitcast_core: rust-patterns
trans-gen-core : development-tools
trans-gen-go : development-tools
trans-gen-java : development-tools
trans-gen-ruby : development-tools
trans-gen-scala : development-tools
trans-schema : development-tools
transit_model: filesystem
trashmap: data-structures
trie-db: rust-patterns
trie-root: data-structures, no-std
trompt: authentication, command-line-interface
trousers-sys: hardware-support
truetype: parser-implementations, rendering::data-formats
truth: parser-implementations
try-guard: rust-patterns
try_from: rust-patterns
tss-tspi: hardware-support
tsukurou_core: development-tools
tt-call: rust-patterns
ttmap: data-structures
ttt: command-line-utilities
ttyaskpass: command-line-interface, authentication
tw_pack_lib: games
tw_unpack: games
twapi: web-programming
tweet: web-programming
typeable: rust-patterns
typedb: database-implementations
typename: rust-patterns
ucd-trie: data-structures, text-processing
ucd-util: text-processing
udp_netmsg: network-programming
ul: rendering::graphics-api
ullage: development-tools
ultrastar-txt: parser-implementations
uploads-im-client: web-programming
urdf-rs: parser-implementations
uriparse: parser-implementations, web-programming
urldecode: web-programming
urlocator: parser-implementations
usereport-rs: development-tools::profiling, command-line-utilities
users: os::unix-apis
utf-8: text-processing, algorithms
utime: filesystem
vad: multimedia::audio
validjson: encoding, command-line-utilities
varlink_parser: parser-implementations
varlociraptor: science
vec3D: science::math
veloren-launcher: games
verba: text-processing, internationalization
versionisator: development-tools::build-tools
vfio-bindings: os::unix-apis
vhdl_lang: development-tools
victoria-dom: parser-implementations, web-programming
videocore: hardware-support, multimedia::video
vsphere-api: web-programming
w_result: rust-patterns
wagyu: cryptography::cryptocurrencies
wake-on-lan: os, network-programming
wcxhead: development-tools
webhook_listener: web-programming::http-server
webidl: parser-implementations
weighted-select: asynchronous
wham: simulation
wikipedia: web-programming
wlambda: parser-implementations
wnck-sys: gui
wooting-sdk: hardware-support
wrc: memory-management
wstring: rust-patterns
x86: hardware-support, os
x86test-types: development-tools::testing
x86test: development-tools::testing
xi-core-lib: text-editors
xio_job_to_blockdiag: command-line-utilities
xml-rs: parser-implementations, encoding
xmltree: parser-implementations
xpc-connection: os::macos-apis
yellowsun: cryptography::cryptocurrencies
ymlctx: config
yoga: gui
yubirs: authentication
zapper_derive: development-tools::procedural-macro-helpers
zemeroth: games
zmq-sys: network-programming
